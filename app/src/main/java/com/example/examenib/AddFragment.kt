package com.example.examenib

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.evernote.android.state.State
import com.evernote.android.state.StateSaver
import kotlinx.android.synthetic.main.fragment_add.*
import org.w3c.dom.Text
import java.util.*
import kotlin.math.sign

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"



class AddFragment : Fragment() {

    private val args: AddFragmentArgs  by navArgs()

    internal lateinit var roundTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var scoreTextView: TextView
    internal lateinit var number1TextView: TextView
    internal lateinit var number2TextView: TextView
    internal lateinit var signTextView: TextView

    internal lateinit var resultText: TextView

    internal lateinit var sendButton: Button

    var score = 0

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private lateinit var countDownTimer: CountDownTimer


    var timeLeft = 10
    var round = 1
    var number1 = 0
    var number2 = 0
    var finalScore = 0


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val sign = args.sign
        roundTextView = view.findViewById(R.id.round_id)
        timeLeftTextView = view.findViewById(R.id.time_left_id)
        scoreTextView = view.findViewById(R.id.score_id)
        number1TextView = view.findViewById(R.id.number1_id)
        number2TextView = view.findViewById(R.id.number2_id)
        signTextView = view.findViewById(R.id.sign_id)
        resultText = view.findViewById(R.id.result_id)
        sendButton = view.findViewById(R.id.send_button)
        signTextView.text = getString(R.string.sign_txt, sign)
        timeLeftTextView.text = getString(R.string.time_left_txt, timeLeft)
        scoreTextView.text = getString(R.string.score_txt, score)
        roundTextView.text = getString(R.string.round_txt, round)



        countDownTimer = object : CountDownTimer(10000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
                timeLeftTextView.text = getString(R.string.time_left_txt, timeLeft)
            }

            override fun onFinish() {
                Toast.makeText(activity, "Finished Time. New Round", Toast.LENGTH_SHORT)
                resetGame()
            }
        }.start()

        resetGame()
        sendButton.setOnClickListener{givePoints()}
    }



    private fun givePoints(){
        var result = 0.0
        var number1Double = number1.toDouble()
        var number2Double = number2.toDouble()

        if (signTextView.text == "+"){
            result = number1Double + number2Double
        }else if (signTextView.text == "-"){
            result = number1Double - number2Double
        }else if (signTextView.text == "*"){
            result = number1Double * number2Double
        }else{
            result = number1Double / number2Double
        }

       var resultFormat = String.format("%.2f", result).toDouble()

       if (resultText.text.toString().toDouble() == resultFormat){
            if (timeLeft <= 10 && timeLeft >= 8){
                score = 100
            }else if (timeLeft < 8 && timeLeft >= 5){
                score = 50
            }else if (timeLeft < 5 && timeLeft >= 0){
                score = 10
            }
        }else{
           score = 0
       }

        Toast.makeText(activity, "Score: ${score}    Time Left: ${timeLeft}", Toast.LENGTH_SHORT).show()
        finalScore = finalScore + score
        round = round + 1
        resetGame()
    }

    fun resetGame(){
        resultText.setText("")
        if(round<=5){
            number1 = (1..10).shuffled().first()
            number2 = (1..10).shuffled().first()
            number1TextView.text = number1.toString()
            number2TextView.text = number2.toString()

            roundTextView.text = getString(R.string.round_txt, round)
            scoreTextView.text = getString(R.string.score_txt, finalScore)
            timeLeftTextView.text = getString(R.string.time_left_txt, timeLeft)

            countDownTimer.start()
        }else{
            sendButton.isClickable = false
            number1TextView.text = ""
            number2TextView.text = ""
            Toast.makeText(activity, "You got ${finalScore} points.", Toast.LENGTH_SHORT).show()
            scoreTextView.text = getString(R.string.score_txt, finalScore)
            countDownTimer.cancel()
        }
    }





    /**
     * A simple [Fragment] subclass.
     * Activities that contain this fragment must implement the
     * [AddFragment.OnFragmentInteractionListener] interface
     * to handle interaction events.
     * Use the [AddFragment.newInstance] factory method to
     * create an instance of this fragment.
     *
     */
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false)
    }
    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }
    override fun onDetach() {
        super.onDetach()
        listener = null
        countDownTimer.cancel()
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Operations.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

