package com.example.examenib

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity(), MainFragment.OnFragmentInteractionListener, AddFragment.OnFragmentInteractionListener{

    override fun onFragmentInteraction(uri: Uri) {

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
