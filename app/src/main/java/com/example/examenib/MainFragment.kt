package com.example.examenib

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.findNavController
import org.w3c.dom.Text


class MainFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnFragmentInteractionListener? = null

    internal lateinit var addButton: Button
    internal lateinit var subButton: Button
    internal lateinit var multButton: Button
    internal lateinit var divButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        addButton = view.findViewById(R.id.add_button)
        addButton.setOnClickListener{goToAddition()}

        subButton = view.findViewById(R.id.sub_button)
        subButton.setOnClickListener{goToSubstraction()}

        multButton = view.findViewById(R.id.mult_button)
        multButton.setOnClickListener{goToMultiplication()}

        divButton = view.findViewById(R.id.div_button)
        divButton.setOnClickListener{goToDivision()}
    }

    fun goToAddition(){
        val action = MainFragmentDirections.actionAddFragment("+")
        view?.findNavController()?.navigate(action)
    }

    fun goToSubstraction(){
        val action = MainFragmentDirections.actionAddFragment("-")
        view?.findNavController()?.navigate(action)
    }
    fun goToMultiplication(){
        val action = MainFragmentDirections.actionAddFragment("*")
        view?.findNavController()?.navigate(action)
    }
    fun goToDivision(){
        val action = MainFragmentDirections.actionAddFragment("/")
        view?.findNavController()?.navigate(action)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

}
